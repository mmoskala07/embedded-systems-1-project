#!/usr/bin/env python
import rospy
import time
import sys
from core2.msg import odometry
import pandas as pd
import numpy as np


class KalmanFilter:
    def __init__(self, A, B, C, initial_state_vector, process_noise_matrix, measurement_noise_matrix):
        self.A = A
        self.B = B
        self.C = C

        self.n = self.A.shape[0]

        self.X = initial_state_vector
        self.P = np.identity(self.n)

        self.Q = process_noise_matrix
        self.R = measurement_noise_matrix

    def predict(self, input):
        self.X = self.A.dot(self.X) + self.B.dot(input)
        self.P = self.A.dot(self.P).dot(self.A.transpose()) + self.Q

    def correct(self, measurement):
        Y = measurement - self.C.dot(self.X)
        S = self.C.dot(self.P).dot(self.C.transpose()) + self.R
        K = self.P.dot(self.C.transpose()).dot(np.linalg.inv(S))

        self.X = self.X + K.dot(Y)
        self.P = np.array(np.identity(self.n)-K.dot(self.C)).dot(self.P)

        return self.X


def odometry_callback(data):
    # Trigger pubisher with filtered data
    odometry_KF_publisher(data)

    if record_and_save_data:
        save_odometry_data_raw(data)

def odometry_KF_publisher(data):
    # Filter data using Kalman Filter
    kf.predict(0)
    kf.correct(np.array([[data.acceleration_x],
                         [data.acceleration_y],
                         [data.acceleration_z],
                         [data.gyro_x],
                         [data.gyro_y],
                         [data.gyro_z]]))
    # Publish filtered data
    data_kf = odometry()
    data_kf.acceleration_x = kf.X[0][0]
    data_kf.acceleration_y = kf.X[1][0]
    data_kf.acceleration_z = kf.X[2][0]
    data_kf.gyro_x = kf.X[3][0]
    data_kf.gyro_y = kf.X[4][0]
    data_kf.gyro_z = kf.X[5][0]
    pub.publish(data_kf)

    if record_and_save_data:
        save_odometry_data_KF(data_kf)

def save_odometry_data_raw(data):
    acc_x.append(data.acceleration_x)
    acc_y.append(data.acceleration_y)
    acc_z.append(data.acceleration_z)
    gyro_x.append(data.gyro_x)
    gyro_y.append(data.gyro_y)
    gyro_z.append(data.gyro_z)

def save_odometry_data_KF(data):
    acc_kf_x.append(data.acceleration_x)
    acc_kf_y.append(data.acceleration_y)
    acc_kf_z.append(data.acceleration_z)
    gyro_kf_x.append(data.gyro_x)
    gyro_kf_y.append(data.gyro_y)
    gyro_kf_z.append(data.gyro_z)


if __name__ == '__main__':
    # Record and save data flag
    record_and_save_data = True
    # Data raw
    acc_x, acc_y, acc_z = [], [], []
    gyro_x, gyro_y, gyro_z = [], [], []
    # Data after KF
    acc_kf_x, acc_kf_y, acc_kf_z = [], [], []
    gyro_kf_x, gyro_kf_y, gyro_kf_z = [], [], []

    # KalmanFilter model
    A = np.diag([1, 1, 1, 1, 1, 1])  # acceleration x, y, z and gyroscope x, y, z
    B = np.array([[0],
                  [0],
                  [0],
                  [0],
                  [0],
                  [0]])
    C = np.identity(6)
    x0 = np.array([[0],
                   [0],
                   [0],
                   [0],
                   [0],
                   [0]])
    Q = B.dot(B.transpose()) * 0
    R = np.diag([0.2, 0.2, 0.2, 5.0, 5.0, 5.0]) # measurement noise variance
    # Create Kalman Filter
    kf = KalmanFilter(A, B, C, x0, Q, R)

    # Initialize node
    rospy.init_node('core2_odometry_recorder', anonymous=False)
    rospy.Subscriber("/core2/odometry", odometry, odometry_callback)
    pub = rospy.Publisher('/core2/odometry_KF', odometry, queue_size=10)
    rate = rospy.Rate(5)

    # Main loop
    while not rospy.is_shutdown():
        if record_and_save_data:
            usage_kB = (sys.getsizeof(acc_x) * 12) / 1024.0
            print("Number of received messages: {:d}\nMemory usage: {:.3f} kB".format(len(acc_x), usage_kB))
        rate.sleep()

    if record_and_save_data:
        # Save data to csv file
        file_name = 'MPU_recorded_data.csv'
        raw_data = {
            "Acc_x": acc_x,
            "Acc_y": acc_y,
            "Acc_z": acc_z,
            "Acc_KF_x": acc_kf_x,
            "Acc_KF_y": acc_kf_y,
            "Acc_KF_z": acc_kf_z,
            "Gyro_x": gyro_x,
            "Gyro_y": gyro_y,
            "Gyro_z": gyro_z,
            "Gyro_KF_x": gyro_kf_x,
            "Gyro_KF_y": gyro_kf_y,
            "Gyro_KF_z": gyro_kf_z
        }
        df = pd.DataFrame(raw_data)
        df.to_csv(file_name)
        print("Data saved to csv file: {:s}".format(file_name))
