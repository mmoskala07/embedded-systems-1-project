#ifndef _ROS_core2_diagnostic_h
#define _ROS_core2_diagnostic_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace core2
{

  class diagnostic : public ros::Msg
  {
    public:
      typedef float _temperature_type;
      _temperature_type temperature;
      typedef bool _fans_activated_type;
      _fans_activated_type fans_activated;
      typedef float _voltage_12V_type;
      _voltage_12V_type voltage_12V;
      typedef float _voltage_24V_type;
      _voltage_24V_type voltage_24V;
      typedef bool _left_limit_switch_pressed_type;
      _left_limit_switch_pressed_type left_limit_switch_pressed;
      typedef bool _right_limit_switch_pressed_type;
      _right_limit_switch_pressed_type right_limit_switch_pressed;
      typedef bool _limit_switches_failure_type;
      _limit_switches_failure_type limit_switches_failure;
      typedef int32_t _mpu9250_status_type;
      _mpu9250_status_type mpu9250_status;

    diagnostic():
      temperature(0),
      fans_activated(0),
      voltage_12V(0),
      voltage_24V(0),
      left_limit_switch_pressed(0),
      right_limit_switch_pressed(0),
      limit_switches_failure(0),
      mpu9250_status(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_temperature;
      u_temperature.real = this->temperature;
      *(outbuffer + offset + 0) = (u_temperature.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_temperature.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_temperature.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_temperature.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->temperature);
      union {
        bool real;
        uint8_t base;
      } u_fans_activated;
      u_fans_activated.real = this->fans_activated;
      *(outbuffer + offset + 0) = (u_fans_activated.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->fans_activated);
      union {
        float real;
        uint32_t base;
      } u_voltage_12V;
      u_voltage_12V.real = this->voltage_12V;
      *(outbuffer + offset + 0) = (u_voltage_12V.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_voltage_12V.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_voltage_12V.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_voltage_12V.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->voltage_12V);
      union {
        float real;
        uint32_t base;
      } u_voltage_24V;
      u_voltage_24V.real = this->voltage_24V;
      *(outbuffer + offset + 0) = (u_voltage_24V.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_voltage_24V.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_voltage_24V.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_voltage_24V.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->voltage_24V);
      union {
        bool real;
        uint8_t base;
      } u_left_limit_switch_pressed;
      u_left_limit_switch_pressed.real = this->left_limit_switch_pressed;
      *(outbuffer + offset + 0) = (u_left_limit_switch_pressed.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->left_limit_switch_pressed);
      union {
        bool real;
        uint8_t base;
      } u_right_limit_switch_pressed;
      u_right_limit_switch_pressed.real = this->right_limit_switch_pressed;
      *(outbuffer + offset + 0) = (u_right_limit_switch_pressed.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->right_limit_switch_pressed);
      union {
        bool real;
        uint8_t base;
      } u_limit_switches_failure;
      u_limit_switches_failure.real = this->limit_switches_failure;
      *(outbuffer + offset + 0) = (u_limit_switches_failure.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->limit_switches_failure);
      union {
        int32_t real;
        uint32_t base;
      } u_mpu9250_status;
      u_mpu9250_status.real = this->mpu9250_status;
      *(outbuffer + offset + 0) = (u_mpu9250_status.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_mpu9250_status.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_mpu9250_status.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_mpu9250_status.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->mpu9250_status);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_temperature;
      u_temperature.base = 0;
      u_temperature.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_temperature.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_temperature.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_temperature.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->temperature = u_temperature.real;
      offset += sizeof(this->temperature);
      union {
        bool real;
        uint8_t base;
      } u_fans_activated;
      u_fans_activated.base = 0;
      u_fans_activated.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->fans_activated = u_fans_activated.real;
      offset += sizeof(this->fans_activated);
      union {
        float real;
        uint32_t base;
      } u_voltage_12V;
      u_voltage_12V.base = 0;
      u_voltage_12V.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_voltage_12V.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_voltage_12V.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_voltage_12V.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->voltage_12V = u_voltage_12V.real;
      offset += sizeof(this->voltage_12V);
      union {
        float real;
        uint32_t base;
      } u_voltage_24V;
      u_voltage_24V.base = 0;
      u_voltage_24V.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_voltage_24V.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_voltage_24V.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_voltage_24V.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->voltage_24V = u_voltage_24V.real;
      offset += sizeof(this->voltage_24V);
      union {
        bool real;
        uint8_t base;
      } u_left_limit_switch_pressed;
      u_left_limit_switch_pressed.base = 0;
      u_left_limit_switch_pressed.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->left_limit_switch_pressed = u_left_limit_switch_pressed.real;
      offset += sizeof(this->left_limit_switch_pressed);
      union {
        bool real;
        uint8_t base;
      } u_right_limit_switch_pressed;
      u_right_limit_switch_pressed.base = 0;
      u_right_limit_switch_pressed.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->right_limit_switch_pressed = u_right_limit_switch_pressed.real;
      offset += sizeof(this->right_limit_switch_pressed);
      union {
        bool real;
        uint8_t base;
      } u_limit_switches_failure;
      u_limit_switches_failure.base = 0;
      u_limit_switches_failure.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->limit_switches_failure = u_limit_switches_failure.real;
      offset += sizeof(this->limit_switches_failure);
      union {
        int32_t real;
        uint32_t base;
      } u_mpu9250_status;
      u_mpu9250_status.base = 0;
      u_mpu9250_status.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_mpu9250_status.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_mpu9250_status.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_mpu9250_status.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->mpu9250_status = u_mpu9250_status.real;
      offset += sizeof(this->mpu9250_status);
     return offset;
    }

    const char * getType(){ return "core2/diagnostic"; };
    const char * getMD5(){ return "81c06b92462f8d9fe421c7ede9cffc88"; };

  };

}
#endif