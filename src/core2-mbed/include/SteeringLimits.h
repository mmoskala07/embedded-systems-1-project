#pragma once

#include "ROS_communication.h"
#include <mbed.h>

using LeftLimitSwitch = InterruptIn;
using RightLimitSwitch = InterruptIn;

class SteeringLimits
{
public:
    SteeringLimits(PinName,
                   PinName,
                   PinName,
                   PinName,
                   DigitalOut&,
                   ROS_Communication&);


private:

    void handleRisingEdgeOnLeftSide();
    void handleRisingEdgeOnRightSide();
    void handleFallingEdgeOnLeftSide();
    void handleFallingEdgeOnRightSide();
    void checkIfStatesOfLeftLimitSwitchesNotMatch();
    void checkIfStatesOfRightLimitSwitchesNotMatch();
    void sendDebugMessage();

    LeftLimitSwitch firstleftLimitSwitch;
    LeftLimitSwitch secondleftLimitSwitch;
    RightLimitSwitch firstRightLimitSwitch;
    RightLimitSwitch secondRightLimitSwitch;
    EventQueue steering_limit_queue;
    int leftCounter = 0;
    int rightCounter = 0;
    
    ROS_Communication& ros_communication;

    DigitalOut LED_steeringLimitReached;
};
