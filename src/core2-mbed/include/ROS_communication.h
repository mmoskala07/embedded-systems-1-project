#pragma once
#include <ros.h>
#include <vector>
#include "ROS_Observer.h"
#include "odometry.h"
#include "diagnostic.h"
#include "commands.h"

class ROS_Communication
{
private:
    ros::Publisher* diag_pub_handler;
    ros::Publisher* odom_pub_handler;
    core2::diagnostic diag_msg;
    core2::odometry odom_msg;
    std::vector<ROS_CommandsObserver*> ros_commands_observers;
public:
    ROS_Communication();
    ROS_Communication(ros::Publisher* diagnostic_publisher, ros::Publisher* odometry_publisher);

    void addCommandsObserver(ROS_CommandsObserver* obj);

    void receiveCommandMessage(const core2::commands& msg);
    void sendDiagnosticMessage(core2::diagnostic msg);
    void sendOdometryMessage(core2::odometry);

    core2::diagnostic getLastSendDiagnosticsMsg();
    core2::odometry getLastSendOdometryMsg();
};