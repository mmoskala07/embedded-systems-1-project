#include <mbed.h>

class BatterySensor
{
private:
    AnalogIn battery12Vadc;
    AnalogIn battery24Vadc;

public:
    BatterySensor(PinName, PinName);
    float get12Voltage();
    float get24Voltage();
};