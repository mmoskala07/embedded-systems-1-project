enum BuzzerState
{
    BUZZER_DISABLED  = 0,
    BUZZER_CONTINOUS = 1,
    BUZZER_FLASHING  = 2
};