#ifndef _ROS_core2_odometry_h
#define _ROS_core2_odometry_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace core2
{

  class odometry : public ros::Msg
  {
    public:
      typedef float _gyro_x_type;
      _gyro_x_type gyro_x;
      typedef float _gyro_y_type;
      _gyro_y_type gyro_y;
      typedef float _gyro_z_type;
      _gyro_z_type gyro_z;
      typedef float _acceleration_x_type;
      _acceleration_x_type acceleration_x;
      typedef float _acceleration_y_type;
      _acceleration_y_type acceleration_y;
      typedef float _acceleration_z_type;
      _acceleration_z_type acceleration_z;
      typedef float _magnetometer_x_type;
      _magnetometer_x_type magnetometer_x;
      typedef float _magnetometer_y_type;
      _magnetometer_y_type magnetometer_y;
      typedef float _magnetometer_z_type;
      _magnetometer_z_type magnetometer_z;

    odometry():
      gyro_x(0),
      gyro_y(0),
      gyro_z(0),
      acceleration_x(0),
      acceleration_y(0),
      acceleration_z(0),
      magnetometer_x(0),
      magnetometer_y(0),
      magnetometer_z(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_gyro_x;
      u_gyro_x.real = this->gyro_x;
      *(outbuffer + offset + 0) = (u_gyro_x.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_gyro_x.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_gyro_x.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_gyro_x.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->gyro_x);
      union {
        float real;
        uint32_t base;
      } u_gyro_y;
      u_gyro_y.real = this->gyro_y;
      *(outbuffer + offset + 0) = (u_gyro_y.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_gyro_y.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_gyro_y.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_gyro_y.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->gyro_y);
      union {
        float real;
        uint32_t base;
      } u_gyro_z;
      u_gyro_z.real = this->gyro_z;
      *(outbuffer + offset + 0) = (u_gyro_z.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_gyro_z.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_gyro_z.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_gyro_z.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->gyro_z);
      union {
        float real;
        uint32_t base;
      } u_acceleration_x;
      u_acceleration_x.real = this->acceleration_x;
      *(outbuffer + offset + 0) = (u_acceleration_x.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_acceleration_x.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_acceleration_x.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_acceleration_x.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->acceleration_x);
      union {
        float real;
        uint32_t base;
      } u_acceleration_y;
      u_acceleration_y.real = this->acceleration_y;
      *(outbuffer + offset + 0) = (u_acceleration_y.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_acceleration_y.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_acceleration_y.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_acceleration_y.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->acceleration_y);
      union {
        float real;
        uint32_t base;
      } u_acceleration_z;
      u_acceleration_z.real = this->acceleration_z;
      *(outbuffer + offset + 0) = (u_acceleration_z.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_acceleration_z.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_acceleration_z.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_acceleration_z.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->acceleration_z);
      union {
        float real;
        uint32_t base;
      } u_magnetometer_x;
      u_magnetometer_x.real = this->magnetometer_x;
      *(outbuffer + offset + 0) = (u_magnetometer_x.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_magnetometer_x.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_magnetometer_x.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_magnetometer_x.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->magnetometer_x);
      union {
        float real;
        uint32_t base;
      } u_magnetometer_y;
      u_magnetometer_y.real = this->magnetometer_y;
      *(outbuffer + offset + 0) = (u_magnetometer_y.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_magnetometer_y.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_magnetometer_y.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_magnetometer_y.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->magnetometer_y);
      union {
        float real;
        uint32_t base;
      } u_magnetometer_z;
      u_magnetometer_z.real = this->magnetometer_z;
      *(outbuffer + offset + 0) = (u_magnetometer_z.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_magnetometer_z.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_magnetometer_z.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_magnetometer_z.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->magnetometer_z);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_gyro_x;
      u_gyro_x.base = 0;
      u_gyro_x.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_gyro_x.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_gyro_x.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_gyro_x.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->gyro_x = u_gyro_x.real;
      offset += sizeof(this->gyro_x);
      union {
        float real;
        uint32_t base;
      } u_gyro_y;
      u_gyro_y.base = 0;
      u_gyro_y.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_gyro_y.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_gyro_y.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_gyro_y.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->gyro_y = u_gyro_y.real;
      offset += sizeof(this->gyro_y);
      union {
        float real;
        uint32_t base;
      } u_gyro_z;
      u_gyro_z.base = 0;
      u_gyro_z.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_gyro_z.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_gyro_z.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_gyro_z.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->gyro_z = u_gyro_z.real;
      offset += sizeof(this->gyro_z);
      union {
        float real;
        uint32_t base;
      } u_acceleration_x;
      u_acceleration_x.base = 0;
      u_acceleration_x.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_acceleration_x.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_acceleration_x.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_acceleration_x.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->acceleration_x = u_acceleration_x.real;
      offset += sizeof(this->acceleration_x);
      union {
        float real;
        uint32_t base;
      } u_acceleration_y;
      u_acceleration_y.base = 0;
      u_acceleration_y.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_acceleration_y.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_acceleration_y.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_acceleration_y.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->acceleration_y = u_acceleration_y.real;
      offset += sizeof(this->acceleration_y);
      union {
        float real;
        uint32_t base;
      } u_acceleration_z;
      u_acceleration_z.base = 0;
      u_acceleration_z.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_acceleration_z.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_acceleration_z.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_acceleration_z.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->acceleration_z = u_acceleration_z.real;
      offset += sizeof(this->acceleration_z);
      union {
        float real;
        uint32_t base;
      } u_magnetometer_x;
      u_magnetometer_x.base = 0;
      u_magnetometer_x.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_magnetometer_x.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_magnetometer_x.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_magnetometer_x.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->magnetometer_x = u_magnetometer_x.real;
      offset += sizeof(this->magnetometer_x);
      union {
        float real;
        uint32_t base;
      } u_magnetometer_y;
      u_magnetometer_y.base = 0;
      u_magnetometer_y.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_magnetometer_y.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_magnetometer_y.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_magnetometer_y.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->magnetometer_y = u_magnetometer_y.real;
      offset += sizeof(this->magnetometer_y);
      union {
        float real;
        uint32_t base;
      } u_magnetometer_z;
      u_magnetometer_z.base = 0;
      u_magnetometer_z.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_magnetometer_z.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_magnetometer_z.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_magnetometer_z.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->magnetometer_z = u_magnetometer_z.real;
      offset += sizeof(this->magnetometer_z);
     return offset;
    }

    const char * getType(){ return "core2/odometry"; };
    const char * getMD5(){ return "6d41420121c5d0dbfeba1c25da302d93"; };

  };

}
#endif