#pragma once
#include "ROS_Observer.h"
#include "mbed.h"

class Fan : public ROS_CommandsObserver
{
private:
    DigitalOut fan_on;

public:
    Fan(PinName);
    void notification(core2::commands);
    void turn_on();
    void turn_off();
    bool fan_working;
};
