#pragma once
#include "mbed.h"
#include "geometry_msgs/Vector3.h"
#include "MPU9250_registers.h"

// https://longnight975551865.wordpress.com/2018/02/11/how-to-read-data-from-mpu9250/
// https://longnight975551865.wordpress.com/2019/11/23/how-to-calibrate-a-magnetometer/
// https://os.mbed.com/teams/BMek/code/MPU9250/file/8a0a9422787d/MPU9250.h/
// https://github.com/bolderflight/MPU9250/blob/master/src/MPU9250.h

class MPU9250
{
private:
    bool isConnected();
    RETURN_CODE readRegisters(REGISTER_ADDR_8BIT address, I2C_DATA* buffer);
    RETURN_CODE writeRegister(REGISTER_ADDR_8BIT address, I2C_DATA value);

    RETURN_CODE setupGeneral();
    RETURN_CODE setupGyroscope();
    RETURN_CODE setupAccelerometer();
    RETURN_CODE setupMagnetometer();

    RETURN_CODE calibrateAccelerometer();
    RETURN_CODE calibrateGyroscope();

    void readAcceleration();   
    void readGyroscope();
    void readMagnetometer();
    void readTemperature();

    float rawDataToMeasurement(I2C_DATA byte_low, I2C_DATA byte_high, SCALE scale);

    SCALE acceleration_scale_g = 2;
    SCALE gyroscope_scale_deg_s = 250;
    SCALE magnetometer_scale = 0;
    SCALE temperature_scale = 125;

    geometry_msgs::Vector3 acceleration;
    geometry_msgs::Vector3 gyroscope;
    geometry_msgs::Vector3 magnetometer;
    float temperature;

    geometry_msgs::Vector3 acc_bias;
    geometry_msgs::Vector3 gyro_bias;

    int requestingPeriod_ms;

public:
    MPU9250(int new_data_period_ms);
    void setup();
    geometry_msgs::Vector3 getAccelerationValues();
    geometry_msgs::Vector3 getGyroscopeValues();
    geometry_msgs::Vector3 getMagnetometerValues();
    float getTemperature();

    RETURN_CODE status;
};