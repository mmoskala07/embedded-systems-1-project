#ifndef _ROS_core2_commands_h
#define _ROS_core2_commands_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace core2
{

  class commands : public ros::Msg
  {
    public:
      typedef int32_t _buzzer_on_type;
      _buzzer_on_type buzzer_on;
      typedef bool _fans_on_type;
      _fans_on_type fans_on;

    commands():
      buzzer_on(0),
      fans_on(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_buzzer_on;
      u_buzzer_on.real = this->buzzer_on;
      *(outbuffer + offset + 0) = (u_buzzer_on.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_buzzer_on.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_buzzer_on.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_buzzer_on.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->buzzer_on);
      union {
        bool real;
        uint8_t base;
      } u_fans_on;
      u_fans_on.real = this->fans_on;
      *(outbuffer + offset + 0) = (u_fans_on.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->fans_on);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_buzzer_on;
      u_buzzer_on.base = 0;
      u_buzzer_on.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_buzzer_on.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_buzzer_on.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_buzzer_on.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->buzzer_on = u_buzzer_on.real;
      offset += sizeof(this->buzzer_on);
      union {
        bool real;
        uint8_t base;
      } u_fans_on;
      u_fans_on.base = 0;
      u_fans_on.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->fans_on = u_fans_on.real;
      offset += sizeof(this->fans_on);
     return offset;
    }

    const char * getType(){ return "core2/commands"; };
    const char * getMD5(){ return "12bec18f17758f58d55b6c58f38d4826"; };

  };

}
#endif