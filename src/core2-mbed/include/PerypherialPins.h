#pragma once
#include <mbed.h>

/*
In this document are defined pins used by peripherial board
HSI and other documents describing perypherial board avaiable in the link below
https://racingagh.sharepoint.com/sites/driverless.dept/Shared%20Documents/Forms/AllItems.aspx?viewid=7c9c829e%2D82d3%2D42f5%2Db227%2D34d23096d029&id=%2Fsites%2Fdriverless%2Edept%2FShared%20Documents%2FElektronika%2FP%C5%82ytka%20dla%20peryferi%C3%B3w
*/

// --- EXT ---
// Interrupt or AnalogIn
// Interrupt or AnalogIn
constexpr PinName MEAS_TEMPERATURE_PIN     = EXT_PIN3; // AnalogIn
constexpr PinName MEAS_24V_PIN             = EXT_PIN4; // AnalogIn
constexpr PinName STEP_MOTOR_CHIP_SELECT   = EXT_PIN5; // StepMotor


// --- SENS3 ---
constexpr PinName LIMIT_SWITCH_LEFT_1_PIN  = SENS3_PIN1; // Interrupt
                                                         // SpecialIO
                                                         // SpecialIO
constexpr PinName RES_GO_SIGNAL_PIN        = SENS3_PIN4; // SpecialIO

// --- SENS4 ---
constexpr PinName LIMIT_SWITCH_LEFT_2_PIN  = SENS4_PIN1; // Interrupt
constexpr PinName ASSI_COLOR_PIN           = SENS4_PIN2; // RawIO
constexpr PinName ASSI_MODE_PIN            = SENS4_PIN3; // RawIO
                                                         // RawIO

// --- SENS5 ---
constexpr PinName LIMIT_SWITCH_RIGHT_1_PIN = SENS5_PIN1; // Interrupt
constexpr PinName ASSI_ACTIVATION_PIN      = SENS5_PIN2; // Relay
constexpr PinName BUZZER_ACTIVATION_PIN    = SENS5_PIN3; // Relay
constexpr PinName BUZZER_MODE_PIN          = SENS5_PIN4; // Relay

// --- SENS6 --- 
constexpr PinName LIMIT_SWITCH_RIGHT_2_PIN = SENS6_PIN1; // Interrupt
constexpr PinName FANS_ACTIVATION_PIN      = SENS6_PIN2; // Relay
                                                         // Relay
                                                         // RawIO

/*
Other pins described below
*/

// LEDS
constexpr PinName MAIN_LED = LED1;
constexpr PinName LIMIT_SWITCH_PRESSED_LED = LED3;

// ENCODERS
#define LEFT_ENCODER_PINS   ENCODER_1
#define RIGHT_ENCODER_PINS  ENCODER_2

