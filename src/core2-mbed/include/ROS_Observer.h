#pragma once
#include "commands.h"


class ROS_CommandsObserver
{
public:
    ROS_CommandsObserver(){};
    ~ROS_CommandsObserver(){};
    virtual void notification(core2::commands){};
    core2::commands receivedMsg;
};

