#pragma once

#include "mbed.h"
#include "ROS_Observer.h"
#include "BuzzerState.h"

class Buzzer : public ROS_CommandsObserver
{
private:
    DigitalOut activationSwitch;
    DigitalOut modeSwitch;

    void setSwitches();

public:
    Buzzer(PinName, PinName);
    void notification(core2::commands);

    BuzzerState currentBuzzerState;
};