#include "SteeringLimits.h"

namespace
{
    constexpr bool LIMIT_REACHED = true;
    constexpr auto LIMIT_SWITCH_CHECKOUT_TIME  = 100;
    Thread steering_limit_thread(osPriorityHigh);
    EventQueue steering_limit_queue;
}


SteeringLimits::SteeringLimits(
    PinName _firstleftLimitSwitch,
    PinName _secondleftLimitSwitch,
    PinName _firstRightLimitSwitch,
    PinName _secondRightLimitSwitch,
    DigitalOut& _steeringLimit_LED,
    ROS_Communication& _ros_communication)
    : firstleftLimitSwitch(_firstleftLimitSwitch, PullDown),
      secondleftLimitSwitch(_secondleftLimitSwitch, PullDown),
      firstRightLimitSwitch(_firstRightLimitSwitch, PullDown),
      secondRightLimitSwitch(_secondRightLimitSwitch, PullDown),
      LED_steeringLimitReached(_steeringLimit_LED),
      ros_communication(_ros_communication)
{
    firstleftLimitSwitch  .rise(steering_limit_queue.event(this, &SteeringLimits::handleRisingEdgeOnLeftSide));
    secondleftLimitSwitch .rise(steering_limit_queue.event(this, &SteeringLimits::handleRisingEdgeOnLeftSide));
    firstRightLimitSwitch .rise(steering_limit_queue.event(this, &SteeringLimits::handleRisingEdgeOnRightSide));
    secondRightLimitSwitch.rise(steering_limit_queue.event(this, &SteeringLimits::handleRisingEdgeOnRightSide));

    firstleftLimitSwitch  .fall(steering_limit_queue.event(this, &SteeringLimits::handleFallingEdgeOnLeftSide));
    secondleftLimitSwitch .fall(steering_limit_queue.event(this, &SteeringLimits::handleFallingEdgeOnLeftSide));
    firstRightLimitSwitch .fall(steering_limit_queue.event(this, &SteeringLimits::handleFallingEdgeOnRightSide));
    secondRightLimitSwitch.fall(steering_limit_queue.event(this, &SteeringLimits::handleFallingEdgeOnRightSide));

    steering_limit_thread.start(callback(&steering_limit_queue, &EventQueue::dispatch_forever));
    // Temporary solution
    // Instead check by function read() state of indicators because on start the indicator can be prossed and steering is actually at limit!
    LED_steeringLimitReached = false;
}


void SteeringLimits::sendDebugMessage(){
    core2::diagnostic msg = ros_communication.getLastSendDiagnosticsMsg();
    msg.limit_switches_failure = true;
    ros_communication.sendDiagnosticMessage(msg);
}

void SteeringLimits::checkIfStatesOfLeftLimitSwitchesNotMatch(){
    if(abs(leftCounter) == 1)    
        sendDebugMessage();
}

void SteeringLimits::checkIfStatesOfRightLimitSwitchesNotMatch(){
    if(abs(rightCounter) == 1)    
        sendDebugMessage();
}

void SteeringLimits::handleRisingEdgeOnLeftSide()
{
    if(abs(++leftCounter) == 1)
        steering_limit_queue.call_in(LIMIT_SWITCH_CHECKOUT_TIME, this, &SteeringLimits::checkIfStatesOfLeftLimitSwitchesNotMatch);
    
    LED_steeringLimitReached = false;
    core2::diagnostic msg = ros_communication.getLastSendDiagnosticsMsg();
    msg.left_limit_switch_pressed = not LIMIT_REACHED;
    ros_communication.sendDiagnosticMessage(msg);
}

void SteeringLimits::handleFallingEdgeOnLeftSide()
{
    if(abs(--leftCounter) == 1)
        steering_limit_queue.call_in(LIMIT_SWITCH_CHECKOUT_TIME, this, &SteeringLimits::checkIfStatesOfLeftLimitSwitchesNotMatch);
    
    LED_steeringLimitReached = true; 
    core2::diagnostic msg = ros_communication.getLastSendDiagnosticsMsg();
    msg.left_limit_switch_pressed = LIMIT_REACHED;
    ros_communication.sendDiagnosticMessage(msg);
}

void SteeringLimits::handleRisingEdgeOnRightSide()
{
    if(abs(++rightCounter) == 1)
       steering_limit_queue.call_in(LIMIT_SWITCH_CHECKOUT_TIME, this, &SteeringLimits::checkIfStatesOfRightLimitSwitchesNotMatch); 
    
    LED_steeringLimitReached = false;
    core2::diagnostic msg = ros_communication.getLastSendDiagnosticsMsg();
    msg.right_limit_switch_pressed = not LIMIT_REACHED;
    ros_communication.sendDiagnosticMessage(msg);
}

void SteeringLimits::handleFallingEdgeOnRightSide()
{
    if(abs(--rightCounter) == 1)
       steering_limit_queue.call_in(LIMIT_SWITCH_CHECKOUT_TIME, this, &SteeringLimits::checkIfStatesOfRightLimitSwitchesNotMatch); 
    
    LED_steeringLimitReached = true;
    core2::diagnostic msg = ros_communication.getLastSendDiagnosticsMsg();
    msg.right_limit_switch_pressed = LIMIT_REACHED;
    ros_communication.sendDiagnosticMessage(msg);
}
