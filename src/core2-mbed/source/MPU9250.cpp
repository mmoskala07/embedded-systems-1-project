#include "MPU9250.h"

namespace
{
    Thread mpu9250_thread(osPriorityLow);
    EventQueue mpu9250_queue;
    I2C i2c(EXT_PIN11, EXT_PIN12); // EXT_PIN11 - SDA, EXT_PIN12 - SCL
}

MPU9250::MPU9250(int new_data_period_ms)
{
    requestingPeriod_ms = new_data_period_ms;
    
    acc_bias.x = 0.0;
    acc_bias.y = 0.0;
    acc_bias.z = 0.0;
    
    gyro_bias.x = 0.0;
    gyro_bias.y = 0.0;
    gyro_bias.z = 0.0;

    temperature = 0.0;
}

void MPU9250::setup()
{
    RETURN_CODE rc;
    if (isConnected())
    {
        rc = setupGeneral();
        if (OK == rc)
            rc = setupAccelerometer();
        if (OK == rc)
            rc = calibrateAccelerometer();
        if (OK == rc)
            rc = setupGyroscope();
        if (OK == rc)
            rc = calibrateGyroscope();
        if (OK == rc)
        {
            mpu9250_thread.start(callback(&mpu9250_queue, &EventQueue::dispatch_forever));
            mpu9250_queue.call_every(requestingPeriod_ms, this, &MPU9250::readAcceleration);
            mpu9250_queue.call_every(requestingPeriod_ms, this, &MPU9250::readGyroscope);
            //mpu9250_queue.call_every(requestingPeriod_ms, this, &MPU9250::readMagnetometer);
            mpu9250_queue.call_every(requestingPeriod_ms*10, this, &MPU9250::readTemperature);
        }
    }
    else
        rc = NOT_CONNECTED;

    status = rc;
}

geometry_msgs::Vector3 MPU9250::getAccelerationValues() {return acceleration;}
geometry_msgs::Vector3 MPU9250::getGyroscopeValues()    {return gyroscope;}
geometry_msgs::Vector3 MPU9250::getMagnetometerValues() {return magnetometer;}
float                  MPU9250::getTemperature()        {return temperature;}

bool MPU9250::isConnected()
{   
    I2C_DATA buffer[1];
    RETURN_CODE rc = readRegisters(WHO_AM_I_MPU9250, buffer);

    if ((OK == rc) && (WHO_AM_I_MPU9250_EXPECTED_RESPONSE == buffer[0]))
        return true;
    else
        return false;
}

RETURN_CODE MPU9250::readRegisters(REGISTER_ADDR_8BIT start_address, I2C_DATA* buffer)
{
    int length = sizeof(buffer) / sizeof(buffer[0]);
    char register_address[1] = {start_address};

    if (0 != i2c.write(MPU9250_ADDR_8BIT, register_address, 1, 1))
        return FAIL_ON_WRITE;
    if (0 != i2c.read(MPU9250_ADDR_8BIT, buffer, length, 0))
        return FAIL_ON_WRITE;
    return OK;
}

RETURN_CODE MPU9250::writeRegister(REGISTER_ADDR_8BIT address, I2C_DATA value)
{
    I2C_DATA data[2] = {address, value};
    int ret = i2c.write(MPU9250_ADDR_8BIT, data, 2, 0);
    if (0 == ret)
        return OK;
    else
        return FAIL_ON_WRITE;    
}

RETURN_CODE MPU9250::setupGeneral()
{
    RETURN_CODE rc;
    // Choose intrnal 20MHz oscillator to get more accurate measurements
    // Wake up device from sleep mode
    rc = writeRegister(PWR_MGMT_1, 0x00);
    wait_ms(100);
    if (OK == rc)
        rc = writeRegister(PWR_MGMT_1, 0x01);

    return rc;
}

RETURN_CODE MPU9250::setupAccelerometer()
{
    RETURN_CODE rc;
    rc = writeRegister(ACC_CONFIG_ADDR, ACC_SCALE_2G);
    if (OK == rc)
        rc = writeRegister(ACC_CONFIG2_ADDR, ACC_INTERAL_LOW_PASS_FILTER_10_2_Hz);
    return rc;
}

RETURN_CODE MPU9250::setupGyroscope()
{
    RETURN_CODE rc;
    rc = writeRegister(GYRO_CONFIG_ADDR, GYRO_SCALE_250_DEG_S);
    if (OK == rc)
        rc = writeRegister(CONFIG_ADDR, GYRO_INTERAL_LOW_PASS_FILTER_10_Hz);
    return rc;
}

RETURN_CODE MPU9250::setupMagnetometer()
{
    return OK;
}

RETURN_CODE MPU9250::calibrateAccelerometer()
{
    RETURN_CODE rc = OK;
    int samples_amount = 500;
    int delay_ms = 20;
    geometry_msgs::Vector3 acc_bias_local;
    acc_bias_local.x = 0;
    acc_bias_local.y = 0;
    acc_bias_local.z = 0;

    for (int i = 0; (i < samples_amount) && (rc == OK); i++)
    {
        readAcceleration();
        acc_bias_local.x += acceleration.x;
        acc_bias_local.y += acceleration.y;
        acc_bias_local.z += acceleration.z;
        wait_ms(delay_ms);
    }
    if (rc == OK)
    {
        acc_bias_local.x /= samples_amount;
        acc_bias_local.y /= samples_amount;
        acc_bias_local.z /= samples_amount;
        acc_bias = acc_bias_local;
    }
    return rc;
}

RETURN_CODE MPU9250::calibrateGyroscope()
{
    RETURN_CODE rc = OK;
    int samples_amount = 500;
    int delay_ms = 20;
    geometry_msgs::Vector3 gyro_bias_local;
    gyro_bias_local.x = 0.0;
    gyro_bias_local.y = 0.0;
    gyro_bias_local.z = 0.0;

    for (int i = 0; (i < samples_amount) && (rc == OK); i++)
    {
        readGyroscope();
        gyro_bias_local.x += gyroscope.x;
        gyro_bias_local.y += gyroscope.y;
        gyro_bias_local.z += gyroscope.z;
        wait_ms(delay_ms);
    }
    if (rc == OK)
    {
        gyro_bias_local.x /= samples_amount;
        gyro_bias_local.y /= samples_amount;
        gyro_bias_local.z /= samples_amount;
        gyro_bias = gyro_bias_local;
    }
    return rc;
}

void MPU9250::readAcceleration()
{
    I2C_DATA buffer[2];
    status = readRegisters(ACC_X_ADDR_HIGH_BYTE, buffer);
    acceleration.x = rawDataToMeasurement(buffer[0], buffer[1], acceleration_scale_g) - acc_bias.x;
    status = readRegisters(ACC_Y_ADDR_HIGH_BYTE, buffer);
    acceleration.y = rawDataToMeasurement(buffer[0], buffer[1], acceleration_scale_g) - acc_bias.y;
    status = readRegisters(ACC_Z_ADDR_HIGH_BYTE, buffer);
    acceleration.z = rawDataToMeasurement(buffer[0], buffer[1], acceleration_scale_g) - acc_bias.z;
}

void MPU9250::readGyroscope()
{
    I2C_DATA buffer[2];
    status = readRegisters(GYRO_X_ADDR_HIGH_BYTE, buffer);
    gyroscope.x = rawDataToMeasurement(buffer[0], buffer[1], gyroscope_scale_deg_s) - gyro_bias.x;
    status = readRegisters(GYRO_Y_ADDR_HIGH_BYTE, buffer);
    gyroscope.y = rawDataToMeasurement(buffer[0], buffer[1], gyroscope_scale_deg_s) - gyro_bias.y;
    status = readRegisters(GYRO_Z_ADDR_HIGH_BYTE, buffer);
    gyroscope.z = rawDataToMeasurement(buffer[0], buffer[1], gyroscope_scale_deg_s) - gyro_bias.z;
}

void MPU9250::readMagnetometer()
{
    I2C_DATA buffer[2];
    status = readRegisters(MAGN_X_ADDR_HIGH_BYTE, buffer);
    magnetometer.x = rawDataToMeasurement(buffer[0], buffer[1], magnetometer_scale);
    status = readRegisters(MAGN_Y_ADDR_HIGH_BYTE, buffer);
    magnetometer.y = rawDataToMeasurement(buffer[0], buffer[1], magnetometer_scale);
    status = readRegisters(MAGN_Z_ADDR_HIGH_BYTE, buffer);
    magnetometer.z = rawDataToMeasurement(buffer[0], buffer[1], magnetometer_scale);
}

void MPU9250::readTemperature()
{
    I2C_DATA buffer[2];
    status = readRegisters(TEMP_HIGH_BYTE, buffer);
    temperature = rawDataToMeasurement(buffer[0], buffer[1], temperature_scale) + 21.0;
}

float MPU9250::rawDataToMeasurement(I2C_DATA byte_low, I2C_DATA byte_high, SCALE scale)
{   
    int16_t rawData = int16_t(int16_t(byte_high << 8) | byte_low);
    float rawDataNormalized = float(rawData) / RAW_DATA_RANGE;
    return rawDataNormalized * scale;
}