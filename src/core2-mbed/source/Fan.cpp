#include "Fan.h"

Fan::Fan(PinName fanPin) : fan_on(fanPin)
{
    fan_on = false;
    fan_working = false;
}

void Fan::notification(core2::commands receivedMsg)
{
    fan_on = receivedMsg.fans_on;
    fan_working = fan_on;
}

void Fan::turn_on()
{
    fan_on = true;
    fan_working = fan_on;
}

void Fan::turn_off()
{
    fan_on = false;
    fan_working = fan_on;
}
