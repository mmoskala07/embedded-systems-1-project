#include "ROS_communication.h"
#include <Thread.h>


ROS_Communication::ROS_Communication()
{}

ROS_Communication::ROS_Communication(ros::Publisher* diagnostic_publisher, ros::Publisher* odometry_publisher)
{
    diag_pub_handler = diagnostic_publisher;
    odom_pub_handler = odometry_publisher;
}

void ROS_Communication::addCommandsObserver(ROS_CommandsObserver* obj)
{
    ros_commands_observers.push_back(obj);
}

void ROS_Communication::receiveCommandMessage(const core2::commands& msg)
{
    for(auto observer = ros_commands_observers.begin(); observer != ros_commands_observers.end(); ++observer)
    {
        (*observer)->notification(msg);
        (*observer)->receivedMsg = msg;
    }
}

void ROS_Communication::sendDiagnosticMessage(core2::diagnostic msg)
{
    diag_pub_handler->publish(&msg);
    diag_msg = msg;
}

void ROS_Communication::sendOdometryMessage(core2::odometry msg)
{
    odom_pub_handler->publish(&msg);
    odom_msg = msg;
}

core2::diagnostic ROS_Communication::getLastSendDiagnosticsMsg()
{
    return diag_msg;
}

core2::odometry ROS_Communication::getLastSendOdometryMsg()
{
    return odom_msg;
}