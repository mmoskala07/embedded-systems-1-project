#include <BatterySensor.h>

namespace
{
    constexpr float VOLTAGE_3_3V = 3.3f;
    constexpr float BIGGER_RESISTOR_OHM = 10000.0f;
    constexpr float SMALLER_RESISTOR_OHM = 1000.0f;
    constexpr float NOMINAL_VOLTAGE = 24.0f;
    constexpr float NOMINAL_CURRENT = NOMINAL_VOLTAGE / (BIGGER_RESISTOR_OHM + SMALLER_RESISTOR_OHM);
    constexpr float NOMINAL_VOLTAGE_ON_RESISTOR = NOMINAL_CURRENT * SMALLER_RESISTOR_OHM;
    constexpr float MEASURED_TO_REAL_VOLTAGE_COEFFICIENT_24V = NOMINAL_VOLTAGE / NOMINAL_VOLTAGE_ON_RESISTOR;
    constexpr float MEASURED_TO_REAL_VOLTAGE_COEFFICIENT_12V = (UPPER_RESISTOR + LOWER_RESISTOR) / LOWER_RESISTOR;
}

BatterySensor::BatterySensor(PinName adc12VPin, PinName adc24VPin) : 
    battery12Vadc(adc12VPin),
    battery24Vadc(adc24VPin)
{}

float BatterySensor::get12Voltage()
{
    return MEASURED_TO_REAL_VOLTAGE_COEFFICIENT_12V * VOLTAGE_3_3V * VIN_MEAS_CORRECTION * battery12Vadc.read();
}

float BatterySensor::get24Voltage()
{
    return MEASURED_TO_REAL_VOLTAGE_COEFFICIENT_24V * VOLTAGE_3_3V * battery24Vadc.read();
}


 