#include "Buzzer.h"
#include "ActivationState.h"


Buzzer::Buzzer(PinName activationPin, PinName modePin) : activationSwitch(activationPin),
                                                         modeSwitch(modePin)
{
    currentBuzzerState = BUZZER_DISABLED;
    setSwitches();
}

void Buzzer::notification(core2::commands receivedMsg)
{
    currentBuzzerState = static_cast<BuzzerState>(receivedMsg.buzzer_on);
    setSwitches();
}

void Buzzer::setSwitches()
{
    switch (currentBuzzerState)
    {
    case BUZZER_DISABLED:
        activationSwitch = DISABLED;
        break;
    case BUZZER_FLASHING:
        activationSwitch = ENABLED;
        modeSwitch = DISABLED;
        break;
    case BUZZER_CONTINOUS:
        activationSwitch = ENABLED;
        modeSwitch = ENABLED;
        break;
    default:
        break;
    }
}