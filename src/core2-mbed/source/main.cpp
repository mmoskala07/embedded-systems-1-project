#include "SteeringLimits.h"
#include "ROS_communication.h"
#include "Buzzer.h"
#include "BatterySensor.h"
#include "MPU9250.h"
#include "PerypherialPins.h"
#include "Fan.h"

namespace
{
    constexpr auto ROS_SEND_DIAGNOSTICS_PERIOD_MS = 1000;
    constexpr auto ROS_SEND_ODOMETRY_PERIOD_MS = 250;
    constexpr auto MAIN_SPIN_PERIOD_MS = 500;

    ros::NodeHandle nh;
    DigitalOut loop_led(MAIN_LED);
    DigitalOut led_limit_reached(LIMIT_SWITCH_PRESSED_LED);
    DigitalOut POWER_5V_SENS(SENS_POWER_ON);
    EventQueue main_queue;
    
    ROS_Communication ros_communication;
    SteeringLimits steeringLimits(LIMIT_SWITCH_LEFT_1_PIN,
                                  LIMIT_SWITCH_LEFT_2_PIN,
                                  LIMIT_SWITCH_RIGHT_1_PIN,
                                  LIMIT_SWITCH_RIGHT_2_PIN,
                                  led_limit_reached,
                                  ros_communication);
    Buzzer buzzer(BUZZER_ACTIVATION_PIN, BUZZER_MODE_PIN);
    BatterySensor battery_voltage(BAT_MEAS, MEAS_24V_PIN);
    MPU9250 imu(ROS_SEND_ODOMETRY_PERIOD_MS);
    Fan fan(FANS_ACTIVATION_PIN);
}

void ROS_Callback(const core2::commands& msg)
{
    // Try to use mbed call with param
    ros_communication.receiveCommandMessage(msg);
}

void ROS_send_diagnostics()
{
    core2::diagnostic msg = ros_communication.getLastSendDiagnosticsMsg();
    msg.temperature = imu.getTemperature();
    msg.fans_activated = fan.fan_working;
    msg.voltage_12V = battery_voltage.get12Voltage();
    msg.voltage_24V = battery_voltage.get24Voltage();
    msg.mpu9250_status = int(imu.status);
    ros_communication.sendDiagnosticMessage(msg);
}

void ROS_send_odometry()
{
    // Keep in mind MPU9250 physical assembly regarding axis values...
    core2::odometry msg;
    geometry_msgs::Vector3 gyro = imu.getGyroscopeValues();
    msg.gyro_x = gyro.x;
    msg.gyro_y = gyro.y;
    msg.gyro_z = gyro.z;
    geometry_msgs::Vector3 acc = imu.getAccelerationValues();
    msg.acceleration_x = acc.x;
    msg.acceleration_y = acc.y;
    msg.acceleration_z = acc.z;
    geometry_msgs::Vector3 mag = imu.getMagnetometerValues();
    msg.magnetometer_x = mag.x;
    msg.magnetometer_y = mag.y;
    msg.magnetometer_z = mag.z;
    
    ros_communication.sendOdometryMessage(msg);
}

void main_spin()
{
    // LED blinking
    loop_led = !loop_led;

    // ROS Communication - spinning
    nh.spinOnce();

    // Temperature and fans
    if(imu.getTemperature() > 40.0)
        fan.turn_on();
    else
        fan.turn_off();
}

int main()
{
    loop_led = 1;
    POWER_5V_SENS = 1;
    // ROS Communication - Initialization
    nh.initNode();
    core2::diagnostic pub_diag_obj;
    core2::odometry pub_odom_obj;
    ros::Publisher pub_diag("/core2/diagnostics", &pub_diag_obj);
    ros::Publisher pub_odom("/core2/odometry", &pub_odom_obj);
    ros::Subscriber<core2::commands> sub_commands("/core2/commands", &ROS_Callback);
    nh.advertise(pub_diag);
    nh.advertise(pub_odom);
    nh.subscribe(sub_commands);
    
    // Communication config
    ros_communication = ROS_Communication(&pub_diag, &pub_odom);
    ros_communication.addCommandsObserver(&buzzer);
    ros_communication.addCommandsObserver(&fan);

    // Setup
    imu.setup();
  
    // Main spinning
    main_queue.call_every(MAIN_SPIN_PERIOD_MS, main_spin);
    main_queue.call_every(ROS_SEND_DIAGNOSTICS_PERIOD_MS, ROS_send_diagnostics);
    main_queue.call_every(ROS_SEND_ODOMETRY_PERIOD_MS, ROS_send_odometry);
    main_queue.dispatch();
}
