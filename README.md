# Embedded Systems Project - low peripherials implementation for autonomous racing car

### Source files of project:  
```sh
\core2\src\core2-mbed\include  
\core2\src\core2-mbed\source  
```

### Custom messages definition:  
```sh
\core2\msg  
```

### ROS node for filtering data:  
```sh
\core2\script  
```